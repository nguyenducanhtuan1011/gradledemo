defaultTasks 'hello2'
logger.info ">>> 1 build.gradle --> This is executed during the configuration phase - timestamp @ ${project.gradle.timestamp()}"

task helloTask

project.helloTask.doLast {
    logger.info 'Hello task'
}

task hello {
    description = "Log the name of the task"
    group = "Welcome"

    doFirst {
        logger.info "My name is: $name and this is my 1nd defined action"
    }
    doLast {
        logger.info "My name is: $name and this is my 2st defined action"
    }
}

hello.doLast {
    logger.info "My name is: $name and this is my 4rd defined action"
}

hello.doLast {
    logger.info "My name is: $name and this is my 3rd defined action"
}

hello.doFirst {
    logger.info "My name is: $name and this is my 5rd defined action"
}

logger.info hello.description
logger.info hello.group

task hello2 {
    description = "Log the name of the task"
    group = "Welcome"
}

hello2.doFirst {
    logger.info "My name is: $name and this is my 1rd defined action"
}.doLast {
    logger.info "My name is: $name and this is my 2rd defined action"
}.doLast {
    logger.info "My name is: $name and this is my 3rd defined action"
}.doLast {
    logger.info "My name is: $name and this is my 4rd defined action"
}.doFirst {
    logger.info "My name is: $name and this is my 5rd defined action"
}

